/*global require, __dirname, console */
var app = require('express')(),
    fs = require('fs'),
    path = require('path');
// Directory containing all datas
var dataDir = __dirname + '/data/';

/**
 * Browse all the files in dirName and sotre it in a map
 * @returns {Object} A map containing the files and their types {subDir{file:{js:true,json:true}}}
 */
var mapDatas = (function () {
    "use strict";
    var map, dirs, i, j, srcPath, files, array, ext, filename;
    console.log("Map data files");
    map = {};
    dirs = fs.readdirSync(dataDir).filter(function (file) {
        return fs.statSync(path.join(dataDir, file)).isDirectory();
    });
    for (i = 0; i < dirs.length; i++) {
        srcPath = dataDir + dirs[i];
        files = fs.readdirSync(srcPath).filter(function (file) {
            return fs.statSync(path.join(srcPath, file)).isFile();
        });
        for (j = 0; j < files.length; j++) {
            array = files[j].split('.');
            ext = array.pop();
            filename = array.shift();
            if (map[dirs[i]] === undefined) {
                map[dirs[i]] = {};
            }
            if (map[dirs[i]][filename] === undefined) {
                map[dirs[i]][filename] = {};
            }

            map[dirs[i]][filename][ext] = true;
        }
    }
    return map;
}());

/**
 * Alias for console.dir() with colors
 * @param {Object} an object to display on console
 */
var inspect = function (obj) {
    "use strict";
    console.dir(obj, {
        depth: null,
        colors: true
    });
};

/**
 * Get datas from files and replace function name with stringify functions
 * @param   {String} type Type of data to get (e.g. races, skills, ...)
 * @returns {Object} Get the data from files
 */
var getDatas = function (type) {
    "use strict";
    var dir, data, items, i, datas = [];
    for (dir in mapDatas) {
        if (mapDatas[dir][type] !== undefined && mapDatas[dir][type].json) {
            data = fs.readFileSync(path.join(dataDir, dir, type + '.json'), {
                encoding: 'utf8'
            });
            items = JSON.parse(data);
            for (i = 0; i < items.length; i++) {
                datas.push(items[i]);
            }
        }
    }
    return datas;
};

app.all('*', function (req, res, next) {
    "use strict";
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.get('/:type', function (req, res) {
    "use strict";
    res.send(getDatas(req.params.type));
});

app.listen(3000, function () {
    "use strict";
    console.log('Express server listening on port 3000');
});
