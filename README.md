# savage-manager

A manager for Savage Worlds

## Getting Started

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Contributing
_(Coming soon)_

## Release History
_(Nothing yet)_

## License
Copyright (c) 2015 Sylvain Bodin  
Licensed under the GPL, v2 licenses.
