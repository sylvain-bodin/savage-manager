/*global angular*/
angular.module('appSW').directive('characterSkill', function () {
    'use strict';
    var calculDiff;
    /**
     * Comptue the difference of skillPoints between oldvalue and new Value.
     * The difference is of 1 point for each step when we are behind the attributeValue and 2 point when we are above
     * @param   {String} oldValue       old value for the skill
     * @param   {String} newValue       new value for the skill
     * @param   {String} attributeValue the value of the attribute, for threshold
     * @returns {Number} the difference of skillPoints
     */
    calculDiff = function (oldValue, newValue, attributeValue) {
        var diff = 0,
            sup = 0,
            inf = 0,
            maxValue = newValue,
            minValue = oldValue,
            reverse = false;

        // Do we go forward or reverse ?
        if (newValue < oldValue) {
            maxValue = oldValue;
            minValue = newValue;
            reverse = true;
        }
        // We start with a first value equal 4 so we put the min to 2 so we keep the step at 2
        if (minValue == 0) {
            minValue = 2;
        }
        // Compute the difference when are above the threshold (the value of the attribute)
        if (maxValue > attributeValue) {
            sup = (maxValue - Math.max(attributeValue, minValue)) * 2;
        }
        // Compute the difference of the residue
        inf = Math.min(maxValue, attributeValue) - Math.min(minValue, attributeValue);
        // Divide by 2 because we have de step of 2// Divide by 2 because we have de step of 2
        diff = (inf + sup) / 2;
        // inverse de sign
        if (reverse) {
            diff = diff * -1;
        }
        return diff;
    };
    return {
        restrict: 'E',
        replace: true,
        scope: {
            skill: '=',
            character: '='
        },
        templateUrl: 'src/common/directive/character-skill/view.html',
        link: function (scope, element, attrs) {
            scope.label = 'skills.' + scope.skill.id + '.label';
            /**
             * Verify if the change of value for the skill is possible.
             * It compute the difference of skill points, and verify if we have enough
             * @param {String} oldValue the old value of the skill
             */
            scope.changeSkill = function (oldValue) {
                var newValue, attributeValue, diff;
                if (oldValue === undefined || oldValue === "") {
                    oldValue = 0;
                }
                newValue = scope.character.skills[scope.skill.id];
                attributeValue = scope.character.attributes[scope.skill.attribute];
                diff = calculDiff(oldValue, newValue, attributeValue);
                if ((scope.character.skillPoints > 0 && scope.character.skillPoints >= Math.abs(diff)) || diff < 0) {
                    scope.character.skillPoints = scope.character.skillPoints - diff;
                } else {
                    scope.character.skills[scope.skill.id] = oldValue;
                }
            };
            /**
             * Reset the value of the skill and revert the use of skill points
             */
            scope.resetSkill = function () {
                var oldValue, newValue, attributeValue, diff;
                oldValue = scope.character.skills[scope.skill.id];
                newValue = 0;
                attributeValue = scope.character.attributes[scope.skill.attribute];
                diff = calculDiff(oldValue, newValue, attributeValue);
                scope.character.skills[scope.skill.id] = 0;
                scope.character.skillPoints = scope.character.skillPoints - diff;
            };
        }
    };
});
