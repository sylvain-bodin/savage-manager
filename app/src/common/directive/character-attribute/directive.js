/*global angular*/
angular.module('appSW').directive('characterAttribute', function () {
    'use strict';
    return {
        restrict: 'E',
        replace: true,
        scope: {
            attribute: '@',
            character: '='
        },
        templateUrl: 'src/common/directive/character-attribute/view.html',
        link: function (scope, element, attrs) {
            scope.label = 'charsheet.attributes.' + attrs.attribute + '.label';
            /**
             * Verify if the change of value for the attribute is possible.
             * It compute the difference of attribute points, and verify if we have enough
             * @param {String} oldValue the old value of the attribute
             */
            scope.changeAttribute = function (oldValue) {
                var newValue, diff;
                newValue = scope.character.attributes[attrs.attribute];
                diff = (newValue - oldValue) / 2;
                if ((scope.character.attributePoints > 0 && scope.character.attributePoints >= Math.abs(diff)) || diff < 0) {
                    scope.character.attributePoints = scope.character.attributePoints - diff;
                } else {
                    scope.character.attributes[attrs.attribute] = oldValue;
                }
            };
        }
    };
});
