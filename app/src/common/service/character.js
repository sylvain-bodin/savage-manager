/*global angular*/
angular.module('appSW').factory('Character', function () {
    'use strict';
    var character = {
        name: "Bill",
        race: "human",
        rank: 1,
        archetype: "",
        xp: 0,
        attributes: {
            strength: 4,
            agility: 4,
            smarts: 4,
            spirit: 4,
            vigor: 4
        },
        attributePoints: 5,
        pace: function () {
            return 6;
        },
        parry: function () {
            var fighting = 0;
            if (this.skills.fighting !== undefined) {
                fighting = this.skills.fighting;
            }
            return (fighting / 2) + 2;
        },
        toughness: function () {
            return (this.attributes.vigor / 2) + 2;
        },
        charisma: function () {
            return 0;
        },
        skills: {},
        skillPoints: 15,
        hindrances: [],
        edges: [],
        notes: "",
        edgePoints: 0
    };

    return {
        getCharacter: function () {
            return character;
        }
    };

});
