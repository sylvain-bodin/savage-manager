/*global angular*/
angular.module('appSW').factory('Datas', function (settings, $resource) {
    'use strict';
    return $resource(settings.endpoint + '/:type');
});
