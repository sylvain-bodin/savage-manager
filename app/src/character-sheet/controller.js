/*global angular*/
angular.module('appSW').controller('CharSheetCtrl', function ($scope, Character, Datas) {
    'use strict';

    var resetCharacter = function () {
        $scope.character = angular.copy(Character.getCharacter());
    };

    $scope.character = angular.copy(Character.getCharacter());
    Datas.query({
        type: 'races'
    }).$promise.then(function (races) {
        $scope.races = races;
    });
    Datas.query({
        type: 'skills'
    }).$promise.then(function (skills) {
        $scope.skills = skills;
    });
    Datas.query({
        type: 'hindrances'
    }).$promise.then(function (hindrances) {
        $scope.hindrances = hindrances;
    });
    Datas.query({
        type: 'edges'
    }).$promise.then(function (edges) {
        $scope.edges = edges;
    });
    /**
     * Apply modification when the race valule is changed.
     * It undo the old race modification and apply the new race modification
     * @param {String} newValue value for the new race
     * @param {String} oldValue value for the old race
     */
    $scope.changeRace = function (newValue) {
        resetCharacter();
        $scope.character.race = newValue;
        if ($scope.races[newValue].traits !== undefined) {
            angular.merge($scope.character, $scope.races[newValue].traits);
        }
    };

});
