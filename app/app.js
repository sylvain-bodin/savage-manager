/*global angular*/
angular.module('appSW', [
    'pascalprecht.translate',
    'ui.router',
    'ngAria',
    'ngAnimate',
    'ui.bootstrap',
    'ngResource'
]).config(function ($translateProvider, $stateProvider, $urlRouterProvider) {
    'use strict';
    $translateProvider.useLoader('$translatePartialLoader', {
        urlTemplate: 'i18n/{lang}/{part}.json'
    });
    $translateProvider.preferredLanguage('fr');
    $translateProvider.cloakClassName('hidden');
    $translateProvider.useSanitizeValueStrategy(null);

    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state('main', {
            abstract: true,
            url: "",
            templateUrl: "src/main.html"
        })
        .state('home', {
            parent: 'main',
            url: "/",
            views: {
                characterSheet: {
                    templateUrl: "src/character-sheet/view.html",
                    controller: 'CharSheetCtrl'
                }
            }
        });

}).run(function ($translatePartialLoader, $translate) {
    'use strict';
    $translatePartialLoader.addPart('common');
    $translatePartialLoader.addPart('character');
    $translatePartialLoader.addPart('races');
    $translatePartialLoader.addPart('skills');

    setTimeout(function () {
        $translate.refresh();
    }, 1);

});
